//
//  TopNavigation.swift
//  BGT
//
//  Created by Macbook Pro 2017 on 9/1/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

class TopNavigation: UIView {
    let kCONTENT_XIB_NAME = "TopNavigation"

    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var lblNotificationNumber: UILabel!
    @IBOutlet weak var btnNotification: UIButton!
    @IBOutlet var contentView: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
        contentView.layer.cornerRadius = 8
        contentView.layer.masksToBounds = true
        
        viewSearch.layer.cornerRadius = 8
        
        btnFilter.layer.cornerRadius = 8
        btnFilter.layer.borderWidth = 1
        btnFilter.layer.borderColor = Constant.Color.blue_dim.cgColor
        
        txtSearch.addPlaceHolder(content: "Nhập trích yếu hoặc số kí hiệu văn bản", color: .gray)
//        viewSwitchToEnglish.layer.cornerRadius = 8
//        viewSwitchToEnglish.layer.masksToBounds = true
//
//        viewSwitchToVietnamese.layer.cornerRadius = 8
//        viewSwitchToEnglish.layer.masksToBounds = true
    }

}
extension UIView
{
    func fixInView(_ container: UIView!) -> Void{
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}
