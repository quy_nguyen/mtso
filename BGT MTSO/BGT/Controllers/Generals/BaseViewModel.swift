//
//  BaseViewModel.swift
//  BGT
//
//  Created by Macbook Pro 2017 on 9/1/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class BaseViewModel {

    lazy var provider: Provider = {
        return Provider()
    }()

    typealias RequestCompletion = ((_ success: Bool, _ error: String?, _ data: Any?) -> (Void))?

    public let loading: PublishSubject<Bool> = PublishSubject()
    
    var cancelHandleResponse: Bool = false
    
    func showLoading() {
        self.cancelHandleResponse = false
        self.loading.onNext(true)
    }
    
    func dismissLoading() {
        self.loading.onNext(false)
    }
    
    func clearCurrentLoading() {
        self.cancelHandleResponse = true
        self.dismissLoading()
    }
}
