//
//  NavigationController.swift
//  BGT
//
//  Created by Nguyen Hoan on 8/20/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {

    @IBOutlet weak var naviBar: UINavigationBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        naviBar.barTintColor = UIColor.white
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UITabBar {

    override open func sizeThatFits(_ size: CGSize) -> CGSize {
    var sizeThatFits = super.sizeThatFits(size)
    sizeThatFits.height = 30 // adjust your size here
    return sizeThatFits
   }
}
