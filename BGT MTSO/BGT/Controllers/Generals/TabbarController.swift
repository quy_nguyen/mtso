//
//  testTabbarController.swift
//  BGT
//
//  Created by Nguyen Hoan on 8/23/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

class testTabbarController: UITabBarController, UISplitViewControllerDelegate {
    var txt: String?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
//
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {

//        tabBarController?.selectedIndex = 2// 5th
//        LeftsideController().delegate = self
    }
    func showLeftTable(){
        let index = selectedIndex
              if(index == 0){
                  let vc: HomeController
                  vc = selectedViewController?.children[0] as! HomeController
//                  vc.firtShowNavi()
              }
              if(index == 1){
                  let vc: ThongKeController
                  vc = selectedViewController?.children[0] as! ThongKeController
                  if(SplitTableController.checkIsHiden){
                      vc.showNaviLeft()
                  } else {
                      vc.hidenNaviLeft()
                      
                  }
              }
              if(index == 2){
                  let vc: UserController
                  vc = selectedViewController?.children[0] as! UserController
                  if(SplitTableController.checkIsHiden){
                      vc.showNaviLeft()
                  } else {
                      vc.hidenNaviLeft()
                      
                  }
              }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension testTabbarController: SelectLeftsideDelegate{
    func showNaviItem() {
         showLeftTable()
               
    }
    
    func textSelected(_ text: String) {
        print("???????????????")
        if(Constant.LeftMenuText.root[0].contains(text)){
                       selectedIndex = 0
                selectedViewController?.children[0].navigationController?.popToRootViewController(animated: false)
        }
       else if(Constant.LeftMenuText.root[1].contains(text)){
            selectedIndex = 0
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "ThongKeVBDieuHanh", bundle: nil)

             let vc = mainstoryboard.instantiateViewController(withIdentifier: "ThongKeVBDieuHanhController") as! ThongKeVBDieuHanhController
            vc.navigationItem.leftBarButtonItems = selectedViewController?.children[0].navigationItem.leftBarButtonItems
            vc.navigationItem.rightBarButtonItems = selectedViewController?.children[0].navigationItem.rightBarButtonItems
            selectedViewController?.children[0].navigationController?.pushViewController(vc, animated: false)
        }
        else if(Constant.LeftMenuText.child_1[0].contains(text)){
            selectedIndex = 0
            let vc = storyboard?.instantiateViewController(withIdentifier: "VBChuaXuLyController") as! VBChuaXuLyController
            vc.navigationItem.leftBarButtonItems = selectedViewController?.children[0].navigationItem.leftBarButtonItems
            vc.navigationItem.rightBarButtonItems = selectedViewController?.children[0].navigationItem.rightBarButtonItems
            selectedViewController?.children[0].navigationController?.pushViewController(vc, animated: false)
        }
        else if(Constant.LeftMenuText.child_1[1].contains(text)){
            selectedIndex = 0
            
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "VBDuThaoChoDuyetKy", bundle: nil)

            let vc = mainstoryboard.instantiateViewController(withIdentifier: "VBDuThaoChoDuyetKyController") as! VBDuThaoChoDuyetKyController
            vc.navigationItem.leftBarButtonItems = selectedViewController?.children[0].navigationItem.leftBarButtonItems
            vc.navigationItem.rightBarButtonItems = selectedViewController?.children[0].navigationItem.rightBarButtonItems
            selectedViewController?.children[0].navigationController?.pushViewController(vc, animated: false)
        }
        else {
//            let vc = storyboard?.instantiateViewController(withIdentifier: "ChiTietVBPDFController") as! ChiTietVBPDFController
//            SplitTableController.checkIsHiden = true
//           splitViewController?.preferredDisplayMode = .primaryHidden
//            SplitTableController.checkIsHiden = true
//
//                    selectedViewController?.children[0].navigationController?.pushViewController(vc, animated: false)
//            //         self.reloadInputViews()

        }
//
    }
    
    
}
extension testTabbarController:  UITabBarControllerDelegate {
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        navigationController?.popToRootViewController(animated: true)
      showLeftTable()
    }
}
