//
//  SideMenu.swift
//  MySideMenu
//
//  Created by Afraz Siddiqui on 7/2/20.
//  Copyright © 2020 ASN GROUP LLC. All rights reserved.
//

import Foundation
import UIKit

protocol MenuControllerDelegate {
//    func didSelectMenuItem(named: SideMenuItem)
}



class MenuController: UITableViewController {

    public var delegate: MenuControllerDelegate?

    var menuItems = Constant.LeftMenuText.root
    var check = Array(repeating: false, count: Constant.LeftMenuText.countTotal)
    init() {
        super.init(nibName: nil, bundle: nil)
        tableView.register(UITableViewCell.self,
                           forCellReuseIdentifier: "IdRootCell")
        tableView.separatorColor = UIColor.white
        
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        tableView.backgroundColor = color
//        view.backgroundColor = color
    }

    // Table

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IdRootCell", for: indexPath)
        cell.textLabel?.text = menuItems[indexPath.row]
        
        if Constant.LeftMenuText.checkRoot(str: menuItems[indexPath.row]) {
            cell.backgroundColor = UIColor(red: 0, green: 255, blue: 255, alpha: 0.1)
            cell.imageView?.image = UIImage(named: "account")
        }else if(Constant.LeftMenuText.checkChild1(str: menuItems[indexPath.row])){
           cell.backgroundColor = UIColor(red: 0, green: 255, blue: 255, alpha: 0.05)
            cell.imageView?.image = UIImage(named: "lock")
        } else{
            cell.backgroundColor = UIColor.white
             cell.imageView?.image = UIImage(named: "icon")
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        check[indexPath.row] = !check[indexPath.row]
        menuItems = Constant.LeftMenuText.resultForLeftMenu(check: check)
        tableView.reloadData()
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if Constant.LeftMenuText.checkRoot(str: menuItems[indexPath.row]) {
            return CGFloat(Constant.LeftMenuText.hightRoot)
        }else if (Constant.LeftMenuText.checkChild1(str: menuItems[indexPath.row])){
            return CGFloat(Constant.LeftMenuText.hightChild)
        } else {
            return CGFloat(Constant.LeftMenuText.hightChildMin)
        }
    }

}
