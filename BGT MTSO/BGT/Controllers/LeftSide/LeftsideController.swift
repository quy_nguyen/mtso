//
//  LeftsideController.swift
//  BGT
//
//  Created by Macbook Pro 2017 on 8/31/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

protocol SelectLeftsideDelegate: class{
    func textSelected(_ text: String)
    func showNaviItem()
}

class LeftsideController: UIViewController {

    @IBOutlet weak var viewBase: UIView!
    @IBOutlet weak var logoItem: UIBarButtonItem!
    @IBOutlet weak var tblLeftMenuContent: UITableView!
    @IBOutlet weak var topSpacing: NSLayoutConstraint!
    @IBOutlet weak var seperatorTopSpacing: NSLayoutConstraint!
    
    var textTable = Constant.LeftMenuText.root
    var leftMenuText = Constant.LeftMenuText.root
    var check = Array(repeating: false, count: Constant.LeftMenuText.countTotal)
    weak var delegate: SelectLeftsideDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        initializeContent()
        // Do any additional setup after loading the view.
    }
    
    
    func initializeContent(){
        setupView()
        
    }
    
    func setupView(){
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.topSpacing.constant = statusBarHeight/2
        self.seperatorTopSpacing.constant = statusBarHeight/2
        viewBase.layer.shadowColor = UIColor.black.cgColor
        viewBase.layer.shadowOpacity = 0.2
        viewBase.layer.shadowOffset = CGSize(width: 5, height: 5);
        viewBase.layer.shadowRadius = 5
        
        logoItem.image = UIImage(named: "logoMTSO_blue")?.withRenderingMode(.alwaysOriginal)
        
        tblLeftMenuContent.register(UINib(nibName: "IdRootCell", bundle: nil), forCellReuseIdentifier: "IdRootCell")
        tblLeftMenuContent.register(UINib(nibName: "IdChild1Cell", bundle: nil), forCellReuseIdentifier: "IdChild1Cell")
        tblLeftMenuContent.register(UINib(nibName: "IdChild2Cell", bundle: nil), forCellReuseIdentifier: "IdChild2Cell")
        
        tblLeftMenuContent.dataSource = self
        tblLeftMenuContent.delegate = self
        
        tblLeftMenuContent.tableFooterView = UIView(frame: .zero)
        
    }
    
    
}
extension LeftsideController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leftMenuText.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if Constant.LeftMenuText.checkRoot(str: leftMenuText[indexPath.row]) {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "IdRootCell",for: indexPath) as? IdRootCell{
            cell.textField.text = leftMenuText[indexPath.row]
            if(leftMenuText[indexPath.row] == Constant.LeftMenuText.root[0]){
                cell.img.image = UIImage(named: "calendar")
            } else if(leftMenuText[indexPath.row] == Constant.LeftMenuText.root[1]) {
                 cell.img.image = UIImage(named: "doc")
            } else {
                 cell.img.image = UIImage(named: "book")
            }
            return cell
        }
//              cell.img.image = UIImage(named:"book")
        }else if (Constant.LeftMenuText.checkChild1(str: leftMenuText[indexPath.row])){
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "IdChild1Cell",for: indexPath) as? IdChild1Cell{
                cell.textField.text = leftMenuText[indexPath.row]
                return cell
            }
        } else {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "IdChild2Cell",for: indexPath) as? IdChild2Cell{
                    cell.textField.text = leftMenuText[indexPath.row]
                    return cell
                }
        }
    
    
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if Constant.LeftMenuText.checkRoot(str: leftMenuText[indexPath.row]) {
            return 60
        }else if (Constant.LeftMenuText.checkChild1(str: leftMenuText[indexPath.row])){
            return 37
        } else {
            return 28
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        check[indexPath.row] = !check[indexPath.row]
        leftMenuText = Constant.LeftMenuText.resultForLeftMenu(check: check)
        tblLeftMenuContent.reloadData()
        let text = leftMenuText[indexPath.row]
        delegate?.textSelected(text)

    }
}
