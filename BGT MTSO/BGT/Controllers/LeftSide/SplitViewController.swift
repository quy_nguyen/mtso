//
//  SplitViewController.swift
//  BGT
//
//  Created by Nguyen Hoan on 8/20/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

class SplitViewController: UISplitViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.maximumPrimaryColumnWidth = UIScreen.main.bounds.width/3.7
    }
}
