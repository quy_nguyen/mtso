//
//  SplitTableController.swift
//  BGT
//
//  Created by Nguyen Hoan on 8/21/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit
protocol SelectLeftMenuDelegate: class{
    func textSelected(_ text: String)
    func showNaviItem()
}
class SplitTableController: UITableViewController {
    static var checkIsHiden = false
    @IBOutlet weak var itemShowTable: UIBarButtonItem!
    @IBOutlet weak var logoItem: UIBarButtonItem!
    @IBOutlet weak var navibar: UINavigationItem!
    weak var delegate: SelectLeftMenuDelegate?
    @IBOutlet var leftMenuTableView: UITableView!
    var textTable = Constant.LeftMenuText.root
    var leftMenuText = Constant.LeftMenuText.root
    var check = Array(repeating: false, count: Constant.LeftMenuText.countTotal)
    override func viewDidLoad() {
        super.viewDidLoad()
        leftMenuTableView.bounces = false
        leftMenuTableView.separatorStyle = .none
        itemShowTable.width = 150
        itemShowTable.image = UIImage(named: "menu_open")
        logoItem.image = UIImage(named: "logoMTSO_blue")?.withRenderingMode(.alwaysOriginal)
        leftMenuTableView.register(UINib(nibName: "IdRootCell", bundle: nil), forCellReuseIdentifier: "IdRootCell")
        leftMenuTableView.register(UINib(nibName: "IdChild1Cell", bundle: nil), forCellReuseIdentifier: "IdChild1Cell")
        leftMenuTableView.register(UINib(nibName: "IdChild2Cell", bundle: nil), forCellReuseIdentifier: "IdChild2Cell")
        leftMenuTableView.dataSource = self
        leftMenuTableView.delegate = self
        splitViewController?.presentsWithGesture = false
    }
    // MARK: - Table view data source

    @IBAction func buttonMenuTapped(_ sender: Any) {
        splitViewController?.preferredDisplayMode = .primaryHidden
        SplitTableController.checkIsHiden = true
        delegate?.showNaviItem()
    
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return leftMenuText.count
    }

   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if Constant.LeftMenuText.checkRoot(str: leftMenuText[indexPath.row]) {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "IdRootCell",for: indexPath) as? IdRootCell{
                cell.textField.text = leftMenuText[indexPath.row]
                if(leftMenuText[indexPath.row] == Constant.LeftMenuText.root[0]){
                    cell.img.image = UIImage(named: "calendar")
                } else if(leftMenuText[indexPath.row] == Constant.LeftMenuText.root[1]) {
                     cell.img.image = UIImage(named: "doc")
                } else {
                     cell.img.image = UIImage(named: "book")
                }
                return cell
            }
//              cell.img.image = UIImage(named:"book")
        }else if (Constant.LeftMenuText.checkChild1(str: leftMenuText[indexPath.row])){
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "IdChild1Cell",for: indexPath) as? IdChild1Cell{
                cell.textField.text = leftMenuText[indexPath.row]
                return cell
            }
        } else {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "IdChild2Cell",for: indexPath) as? IdChild2Cell{
                    cell.textField.text = leftMenuText[indexPath.row]
                    return cell
                }
        }
        
        
        return UITableViewCell()

    }
   

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        check[indexPath.row] = !check[indexPath.row]
        leftMenuText = Constant.LeftMenuText.resultForLeftMenu(check: check)
        leftMenuTableView.reloadData()
        let text = leftMenuText[indexPath.row]
//        print(text)
        delegate?.textSelected(text)
//        if
//          let detailViewController = delegate as? testTabbarController,
//          let detailNavigationController = detailViewController.navigationController {
//            splitViewController?.showDetailViewController(detailNavigationController, sender: nil)
//            print("comple")
//        }
        
       

        
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if Constant.LeftMenuText.checkRoot(str: leftMenuText[indexPath.row]) {
            return 60
        }else if (Constant.LeftMenuText.checkChild1(str: leftMenuText[indexPath.row])){
            return 37
        } else {
            return 28
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
