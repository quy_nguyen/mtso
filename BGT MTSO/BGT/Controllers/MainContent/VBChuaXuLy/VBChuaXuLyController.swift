//
//  VBChuaXuLyController.swift
//  BGT
//
//  Created by Nguyen Hoan on 8/26/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

class VBChuaXuLyController: UIViewController {
    
    @IBOutlet weak var vHeader: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var tblContent: UITableView!
    @IBOutlet weak var vYear: UIView!
    @IBOutlet weak var vNumber: UIView!
    @IBOutlet weak var vNumberCode: UIView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtNumber: UITextField!
    @IBOutlet weak var txtYear: UITextField!
    @IBOutlet weak var navBotConstant: NSLayoutConstraint!
    @IBOutlet weak var navHeight: NSLayoutConstraint!
    @IBOutlet weak var txtNumberCode: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeContent()
    }
    
    func initializeContent(){
        
        setupView()
    }
    
    func setupView(){
        self.view.layer.backgroundColor = Constant.Color.hex_F9FCFE.cgColor
        self.navHeight.constant = self.navigationBarHeight
        self.navBotConstant.constant = self.statusBarHeight/2
        
        self.vHeader.layer.borderWidth = 1
        self.vHeader.layer.borderColor = Constant.Color.border.cgColor
        self.vYear.layer.cornerRadius = 15
        self.vNumber.layer.cornerRadius = 15
        self.vNumberCode.layer.cornerRadius = 15
        self.btnSearch.layer.cornerRadius = 15
        self.btnSearch.layer.borderWidth = 2
        self.btnSearch.layer.borderColor = Constant.Color.hex_006DD2.cgColor
                
        let VBChuaXuLyCell = UINib(nibName: "VBChuaXuLyCell", bundle: nil)
        self.tblContent.register(VBChuaXuLyCell, forCellReuseIdentifier: "VBChuaXuLyCell")
        self.tblContent.separatorStyle = .none
        self.tblContent.delegate = self
        self.tblContent.dataSource = self
        
        
    }
}

extension VBChuaXuLyController:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 5
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier:"VBChuaXuLyCell", for: indexPath) as! VBChuaXuLyCell
       cell.selectionStyle = .none
      
       
       return cell

    }
}

