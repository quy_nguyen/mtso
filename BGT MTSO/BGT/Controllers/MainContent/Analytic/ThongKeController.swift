//
//  testThongkeController.swift
//  BGT
//
//  Created by Nguyen Hoan on 8/23/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

class ThongKeController: UIViewController {
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchItem: UIBarButtonItem!
    @IBOutlet weak var itemShowTable: UIBarButtonItem!
    @IBOutlet weak var logoItem: UIBarButtonItem!
    @IBOutlet weak var num: UIBarButtonItem!
//    @IBAction func tap(_ sender: Any) {
//      let vc = storyboard?.instantiateViewController(withIdentifier: "testviewpushController") as! testviewpushController
//        navigationController?.pushViewController(vc,
//                                                 animated: true)
//    }
    
    @IBAction func showLeftTable(_ sender: Any) {
        splitViewController?.preferredDisplayMode = .allVisible
        SplitTableController.checkIsHiden = false
       firtShowNavi()
    }
    func hidenNaviLeft(){
        logoItem.image = nil
        logoItem.title = nil
        logoItem.width = 0
        itemShowTable.image = nil
        itemShowTable.title = nil
        itemShowTable.width = 0
        searchField.width(constant: 500)
    }
    func showNaviLeft(){
//        logoItem.image = UIImage(named: "logoMTSO_blue")
        logoItem.title = nil
        logoItem.width = 250
        itemShowTable.image = UIImage(named: "menu_close")
        itemShowTable.title = nil
//        itemShowTable.width = 0
        logoItem.image = UIImage(named: "logoMTSO_blue")?.withRenderingMode(.alwaysOriginal)
        itemShowTable.width = 100
    }
    func next(){
//        let vc = storyboard?.instantiateViewController(withIdentifier: "ChartController") as! ChartController
//        navigationController?.pushViewController(vc,
//                                                 animated: true)
    }
    func firtShowNavi(){
        if(SplitTableController.checkIsHiden){
            showNaviLeft()
        } else {
            hidenNaviLeft()
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        firtShowNavi()
        
    }
    

}

