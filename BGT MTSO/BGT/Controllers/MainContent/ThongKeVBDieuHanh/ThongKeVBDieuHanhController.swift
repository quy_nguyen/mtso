//
//  testviewpushController.swift
//  BGT
//
//  Created by Nguyen Hoan on 8/24/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

class ThongKeVBDieuHanhController: UIViewController {

    @IBOutlet weak var vLichCongTac: UIView!
    @IBOutlet weak var vVanBanDieuHanh: UIView!
    @IBOutlet weak var vNhiemVuChinhPhu: UIView!
    @IBOutlet weak var vThongKeVBChoDuyetKy: UIView!
    @IBOutlet weak var vChart: UIView!
    @IBOutlet weak var lblVBChinhPhu: UILabel!
    @IBOutlet weak var lblNumberVBChinhPhu: UILabel!
    @IBOutlet weak var lblVBBoGTVT: UILabel!
    @IBOutlet weak var lblNumberVBBoGTVT: UILabel!
    @IBOutlet weak var lblGiayMoi: UILabel!
    @IBOutlet weak var lblNumberGiayMoi: UILabel!
    @IBOutlet weak var lblVBBoUBND: UILabel!
    @IBOutlet weak var lblNumberVBBoUBND: UILabel!
    @IBOutlet weak var lblVBSoHuyen: UILabel!
    @IBOutlet weak var lblNumberSoHuyen: UILabel!
    @IBOutlet weak var lblVBKhac: UILabel!
    @IBOutlet weak var lblNumberVBKhac: UILabel!
    @IBOutlet weak var lblNumberVBChoDuyet: UILabel!
    @IBOutlet weak var lblNumberVBChoKy: UILabel!
    @IBOutlet weak var navBotConstant: NSLayoutConstraint!
    @IBOutlet weak var navHeight: NSLayoutConstraint!
    
    @IBOutlet weak var chart: PieChartView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeContent()

        
    }
    
    func initializeContent(){
        setupView()
    }
    
    func setupView(){
        self.view.layer.backgroundColor = Constant.Color.hex_F9FCFE.cgColor

        self.navHeight.constant = self.navigationBarHeight
        self.navBotConstant.constant = self.statusBarHeight/2
        self.vVanBanDieuHanh.layer.cornerRadius = 15
        self.vNhiemVuChinhPhu.layer.cornerRadius = 15
        self.vLichCongTac.layer.cornerRadius = 15
        self.vThongKeVBChoDuyetKy.layer.borderWidth = 1
        self.vThongKeVBChoDuyetKy.layer.borderColor = Constant.Color.border.cgColor
        self.vChart.layer.borderWidth = 1
        self.vChart.layer.borderColor = Constant.Color.border.cgColor

        
        chart.segments = [
            Segment(color: Constant.Color.hex_4FA0CA, value: 10),
            Segment(color: Constant.Color.hex_6E77FD, value: 10),
            Segment(color: Constant.Color.hex_727383, value: 10),
            Segment(color: Constant.Color.hex_DAAF9E, value: 30),
            Segment(color: Constant.Color.hex_FD614A, value: 20),
            Segment(color: Constant.Color.hex_FEA621, value: 20)

        ]

    }



}
