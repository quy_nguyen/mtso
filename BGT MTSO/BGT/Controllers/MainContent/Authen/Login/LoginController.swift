//
//  LoginController.swift
//  BGT
//
//  Created by HoanMata on 8/6/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit
import BiometricAuthentication
import SVProgressHUD
import RxCocoa
import RxSwift

class LoginController: UIViewController {

    let loginModel = LoginViewModel()
    let disposeBag = DisposeBag()

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginFingerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeComponent()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkUserLoggedIn()
    }
    
    func initializeComponent() {
        setupBinding()
        setupView()
    }
    
    func setupView(){
        loginFingerButton.backgroundColor = UIColor.clear
        loginFingerButton.layer.cornerRadius = 4
        loginFingerButton.layer.borderWidth = 1
        loginFingerButton.layer.borderColor = UIColor.white.cgColor
        loginButton.layer.cornerRadius = 4
        // Do any additional setup after loading the view.
        let imgUsername:UIImage! = UIImage(named: "account")
        let imgPassword:UIImage! = UIImage(named: "lock")
        addLeftImageTo(txtField: usernameTextField, andImage: imgUsername)
        addLeftImageTo(txtField: passwordTextField, andImage: imgPassword)
    }
    @IBAction func checkBoxTapped(_ sender: UIButton) {
                if sender.isSelected{
                    sender.isSelected = false
                } else{
                    sender.isSelected = true
                }
    }
    @IBAction func loginTapped(_ sender: Any) {
        login()
    }
    @IBAction func loginFingerTapped(_ sender: Any) {
        // Set AllowableReuseDuration in seconds to bypass the authentication when user has just unlocked the device with biometric
        BioMetricAuthenticator.shared.allowableReuseDuration = 30
        
        // start authentication
        BioMetricAuthenticator.authenticateWithBioMetrics(reason: "") { [weak self] (result) in
                
            switch result {
            case .success( _):
                
                // authentication successful
                self?.showLoginSucessAlert()
                
            case .failure(let error):
                
                switch error {
                    
                // device does not support biometric (face id or touch id) authentication
                case .biometryNotAvailable:
                    self?.showErrorAlert(message: error.message())
                    
                // No biometry enrolled in this device, ask user to register fingerprint or face
                case .biometryNotEnrolled:
                    self?.showGotoSettingsAlert(message: error.message())
                    
                // show alternatives on fallback button clicked
                case .fallback:
                    self?.usernameTextField.becomeFirstResponder() // enter username password manually
                    
                    // Biometry is locked out now, because there were too many failed attempts.
                // Need to enter device passcode to unlock.
                case .biometryLockedout:
                    self?.showPasscodeAuthentication(message: error.message())
                    
                // do nothing on canceled by system or user
                case .canceledBySystem, .canceledByUser:
                    break
                    
                // show error for any other reason
                default:
                    self?.showErrorAlert(message: error.message())
                }
            }
        }
    }
    func showPasscodeAuthentication(message: String) {
        
        BioMetricAuthenticator.authenticateWithPasscode(reason: message) { [weak self] (result) in
            switch result {
            case .success( _):
                self?.showLoginSucessAlert() // passcode authentication success
            case .failure(let error):
                print(error.message())
            }
        }
    }
    func addLeftImageTo(txtField: UITextField, andImage img: UIImage) {
        let leftImageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: img.size.width, height: img.size.height))
        leftImageView.image = img
        txtField.leftView = leftImageView
        txtField.leftViewMode = .always
    }
    
    func checkUserLoggedIn(){
        if UserDefaultUtils.shared.getAccessToken() != nil {
//            setRootViewAsHome()
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    func login(){
        let username = usernameTextField.text!
        let password = passwordTextField.text!
        
        //tunglt
        //123456a@A
        
        let request = LoginRequest(client_id: "PM-003",
                                   client_secret: "1d5a4346-6050-4b91-b799-d93a6409e70f",
                                   username: username,
                                   password: password,
                                   grant_type: "password")
        
        loginModel.login(request: request)
        
    }
    
    private func setupBinding() {
        loginModel.loading.observeOn(MainScheduler.instance).subscribe(onNext: {
            (loading) in
            if loading {
                SVProgressHUD.show()
            } else {
                SVProgressHUD.dismiss()
            }
        }).disposed(by: disposeBag)
        
        loginModel.errorMsg.observeOn(MainScheduler.instance).subscribe(onNext: {
              (errorMessage) in
            print(errorMessage)
        }).disposed(by: disposeBag)
        
        
        loginModel.accessToken.observeOn(MainScheduler.instance).subscribe(onNext: {
              (accessToken) in
            print(accessToken)
            UserDefaultUtils.shared.saveAccessToken(token: accessToken)
            self.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
    }
}

// MARK: - Alerts
extension LoginController {
    
    func showAlert(title: String, message: String) {
        
        let okAction = AlertAction(title: OKTitle)
        let alertController = getAlertViewController(type: .alert, with: title, message: message, actions: [okAction], showCancel: false) { (button) in
        }
        present(alertController, animated: true, completion: nil)
    }
    
    func showLoginSucessAlert() {
        showAlert(title: "Success", message: "Login successful")
    }
    
    func showErrorAlert(message: String) {
        showAlert(title: "Error", message: message)
    }
    
    func showGotoSettingsAlert(message: String) {
        let settingsAction = AlertAction(title: "Go to settings")
        
        let alertController = getAlertViewController(type: .alert, with: "Error", message: message, actions: [settingsAction], showCancel: true, actionHandler: { (buttonText) in
            if buttonText == CancelTitle { return }
            
            // open settings
            let url = URL(string: UIApplication.openSettingsURLString)
            if UIApplication.shared.canOpenURL(url!) {
                UIApplication.shared.open(url!, options: [:])
            }
            
        })
        present(alertController, animated: true, completion: nil)
    }
}
