//
//  AccountViewModel.swift
//  BGT
//
//  Created by Quý Nguyễn on 9/1/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import SwiftyJSON
import Alamofire
class AccountViewModel: BaseViewModel {
    
    // MARK: - Input
    public let errorMsg: PublishSubject<String> = PublishSubject()
    public let accessToken: PublishSubject<String> = PublishSubject()
    
    public func getOfficialsInfo(request: LoginRequest) {
        self.loading.onNext(true)

        provider.requestAPIJSON(api: .login, parameters: request.setParams(),encoding: URLEncoding()) { (success, IsFailResponseError, data) -> (Void) in
            self.loading.onNext(false)
            if success && !IsFailResponseError , let data = data {
                print(" login: ",data)
                guard let jsonData = try? JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted) else {return}
                let decoder = JSONDecoder()
                do {
                    let loginResponse = try decoder.decode(LoginResponse.self, from: jsonData)
                    guard let token = loginResponse.access_token else {return}
                    print("================token :")
//                    self.accessToken.onNext(token)
                } catch {
                    print(error.localizedDescription)
                }
            } else if !success && !IsFailResponseError, let data = data {

//                guard let jsonData = try? JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted) else {return}
//                let errorModel = try? JSONDecoder().decode(Error.self, from: jsonData) as Error
//                self.errorMsg.onNext(errorModel?.entityName ?? "")
                
            } else {
//                self.errorMsg.onNext(dLocalized("KEY_DEFAULT_INTERNET_ERROR"))

    //                self.errorMsg.onNext("kDefaultInternetError".localized)
            }
        }
    }
    
}
