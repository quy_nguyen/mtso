//
//  TestUserController.swift
//  BGT
//
//  Created by Nguyen Hoan on 8/20/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import RxCocoa
import RxSwift

class UserController: UIViewController  {
  
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var searchField: UITextField!
        @IBOutlet weak var itemShowTable: UIBarButtonItem!
        @IBOutlet weak var logoItem: UIBarButtonItem!
    @IBOutlet weak var viewDisplayInfor: UIView!
    @IBOutlet weak var viewInfor: UIView!
    
    @IBOutlet weak var viewUser: UIView!
    @IBOutlet weak var vAvatar: UIView!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    
    
    @IBOutlet weak var heightViewInfo: NSLayoutConstraint!
    @IBOutlet weak var btnShowInfor: UIButton!
    @IBOutlet weak var btnEditInfor: UIButton!
    @IBOutlet weak var txtDisplayName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPosition: SkyFloatingLabelTextField!
    @IBOutlet weak var txtStaffCode: SkyFloatingLabelTextField!
    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtDepartment: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSaveInfo: UIButton!
    @IBOutlet weak var btnCancelInfo: UIButton!
    @IBOutlet weak var vBtnInfo: UIView!
    
    @IBOutlet weak var viewPassWord: UIView!
    @IBOutlet weak var viewChangePassword: UIView!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var heightViewPassword: NSLayoutConstraint!
    @IBOutlet weak var txtOldPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnOldPassword: UIButton!
    @IBOutlet weak var txtNewPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnNewPassword: UIButton!
    @IBOutlet weak var txtConfirmPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnConfirmPassword: UIButton!
    @IBOutlet weak var btnUpdatePassword: UIButton!
    @IBOutlet weak var btnCancelPassword: UIButton!
    
    @IBOutlet weak var viewSignature: UIView!
    @IBOutlet weak var viewChildSignature: UIView!
    @IBOutlet weak var heightViewSignature: NSLayoutConstraint!
    @IBOutlet weak var btnSignature: UIButton!
    
    @IBOutlet weak var heightSettingDefault: NSLayoutConstraint!
    @IBOutlet weak var viewSettingDefault: UIView!
    @IBOutlet weak var viewChildSettingDefault: UIStackView!
    @IBOutlet weak var btnSettingDefault: UIButton!
    
    @IBOutlet weak var heightSoftwareSetting: NSLayoutConstraint!
    @IBOutlet weak var viewSoftwareSetting: UIView!
    @IBOutlet weak var viewChildSoftwareSetting: UIView!
    @IBOutlet weak var btnSoftSetting: UIButton!
    
    let standardViewHieght: CGFloat = 80
    let disposeBag = DisposeBag()
    let accountViewModel = AccountViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firtShowNavi()
        setupView() 
        setupBinding()
        handlerAction()
        
    }
    
    func setupView() {
        setupViewStandard(view: viewDisplayInfor)
        setupViewStandard(view: viewPassWord)
        setupViewStandard(view: viewSignature)
        setupViewStandard(view: viewSettingDefault)
        setupViewStandard(view: viewSoftwareSetting)
        
        setupViewHeader()
        setupViewInfor()
        setupViewPassword()
        setupViewSignature()
        setupViewDefaultSetting()
        setupViewSoftwareSetting()
    }
    
    func setupViewHeader() {
        vAvatar.layer.borderWidth = 10
        vAvatar.layer.borderColor = UIColor.white.cgColor
        vAvatar.layer.cornerRadius = 90
        vAvatar.layer.masksToBounds = true
        
        vAvatar.layer.shadowColor = UIColor.gray.cgColor
        vAvatar.layer.shadowOpacity = 2
        vAvatar.layer.shadowRadius = 2
        vAvatar.layer.shadowOffset = .zero
               
        btnCamera.alpha = 0.7
    }
    
    func setupViewInfor() {
        viewInfor.isHidden = true
        vBtnInfo.isHidden = true
        heightViewInfo.constant = standardViewHieght
        
        let padding: CGFloat = 15
        
        txtDisplayName.addPadding(padding: .left(padding))
        txtPosition.addPadding(padding: .left(padding))
        txtStaffCode.addPadding(padding: .left(padding))
        txtName.addPadding(padding: .left(padding))
        txtDepartment.addPadding(padding: .left(padding))
        txtEmail.addPadding(padding: .left(padding))
        
        btnSaveInfo.layer.cornerRadius = 5
        
        btnCancelInfo.layer.cornerRadius = 5
        btnCancelInfo.layer.borderWidth = 1
        btnCancelInfo.layer.borderColor = UIColor(red: 0/255, green: 109/255, blue: 210/255, alpha: 1).cgColor
    }
    
    func setupViewPassword() {
        viewChangePassword.isHidden = true
        heightViewPassword.constant = standardViewHieght
        
        let padding: CGFloat = 15
        
        txtOldPassword.addPadding(padding: .left(padding))
        txtNewPassword.addPadding(padding: .left(padding))
        txtConfirmPassword.addPadding(padding: .left(padding))
        
        btnUpdatePassword.layer.cornerRadius = 5
        
        btnCancelPassword.layer.cornerRadius = 5
        btnCancelPassword.layer.borderWidth = 1
        btnCancelPassword.layer.borderColor = UIColor(red: 0/255, green: 109/255, blue: 210/255, alpha: 1).cgColor

    }
    
    func setupViewSignature() {
        viewChildSignature.isHidden = true
        heightViewSignature.constant = standardViewHieght
    }
    
    func setupViewDefaultSetting() {
        viewChildSettingDefault.isHidden = true
        heightSettingDefault.constant = standardViewHieght
    }
    
    func setupViewSoftwareSetting() {
        viewChildSoftwareSetting.isHidden = true
        heightSoftwareSetting.constant = standardViewHieght
    }
    
    func setupViewStandard(view: UIView) {
        view.layer.cornerRadius = 25
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 2
        view.layer.shadowRadius = 2
        view.layer.shadowOffset = .zero
    }
    
    func setupBinding() {
        
    }
    
    func handlerAction(){
        self.btnShowInfor.rx.tap.asDriver().throttle(RxTimeInterval.milliseconds(100)).drive(onNext: { [weak self] in
            if self!.viewInfor.isHidden {
                self!.viewInfor.isHidden = false
                self!.heightViewInfo.constant = 330
                self!.vBtnInfo.isHidden = true
            } else {
                self!.viewInfor.isHidden = true
                self!.heightViewInfo.constant = self!.standardViewHieght
            }
        }).disposed(by: disposeBag)
        
        self.btnEditInfor.rx.tap.asDriver().throttle(RxTimeInterval.milliseconds(100)).drive(onNext: { [weak self] in
            self!.btnEditInfor.isHidden = true
            self!.btnShowInfor.isHidden = true
            self!.viewInfor.isHidden = false
            self!.heightViewInfo.constant = 370
            self!.vBtnInfo.isHidden = false
        }).disposed(by: disposeBag)
        
        self.btnSaveInfo.rx.tap.asDriver().throttle(RxTimeInterval.milliseconds(100)).drive(onNext: { [weak self] in
            
        }).disposed(by: disposeBag)
        
        self.btnCancelInfo.rx.tap.asDriver().throttle(RxTimeInterval.milliseconds(100)).drive(onNext: { [weak self] in
            self!.btnEditInfor.isHidden = false
            self!.btnShowInfor.isHidden = false
            self!.viewInfor.isHidden = false
            self!.heightViewInfo.constant = 330
            self!.vBtnInfo.isHidden = true
        }).disposed(by: disposeBag)
        
        self.btnSignature.rx.tap.asDriver().throttle(RxTimeInterval.milliseconds(100)).drive(onNext: { [weak self] in
            if self!.viewChildSignature.isHidden {
                self!.viewChildSignature.isHidden = false
                self!.heightViewSignature.constant = 520
            } else {
                self!.viewChildSignature.isHidden = true
                self!.heightViewSignature.constant = self!.standardViewHieght
            }
        }).disposed(by: disposeBag)
        
        self.btnSettingDefault.rx.tap.asDriver().throttle(RxTimeInterval.milliseconds(100)).drive(onNext: { [weak self] in
            if self!.viewChildSettingDefault.isHidden {
                self!.viewChildSettingDefault.isHidden = false
                self!.heightSettingDefault.constant = 240
            } else {
                self!.viewChildSettingDefault.isHidden = true
                self!.heightSettingDefault.constant = self!.standardViewHieght
            }
        }).disposed(by: disposeBag)
        
        self.btnChangePassword.rx.tap.asDriver().throttle(RxTimeInterval.milliseconds(100)).drive(onNext: { [weak self] in
            if self!.viewChangePassword.isHidden {
                self!.viewChangePassword.isHidden = false
                self!.heightViewPassword.constant = 380
            } else {
                self!.viewChangePassword.isHidden = true
                self!.heightViewPassword.constant = self!.standardViewHieght
            }
        }).disposed(by: disposeBag)
        
        self.btnSoftSetting.rx.tap.asDriver().throttle(RxTimeInterval.milliseconds(100)).drive(onNext: { [weak self] in
            if self!.viewChildSoftwareSetting.isHidden {
                self!.viewChildSoftwareSetting.isHidden = false
                self!.heightSoftwareSetting.constant = 250
            } else {
                self!.viewChildSoftwareSetting.isHidden = true
                self!.heightSoftwareSetting.constant = self!.standardViewHieght
            }
        }).disposed(by: disposeBag)

        
//        self.viewCamera.rx.tapGesture().when(.recognized).subscribe(onNext: {_ in
//
//            let imagePickerController = UIImagePickerController()
//            imagePickerController.delegate = self
//
//            if UIImagePickerController.isSourceTypeAvailable(.camera){
//                imagePickerController.sourceType = .camera
//                self.present(imagePickerController, animated: true, completion: nil)
//            }else{
//                print("camera not available")
//            }
//
//        }).disposed(by:disposeBag)
    }
    
        @IBAction func showLeftTable(_ sender: Any) {
            splitViewController?.preferredDisplayMode = .allVisible
            SplitTableController.checkIsHiden = false
            firtShowNavi()
        }
    
        func hidenNaviLeft(){
            logoItem.image = nil
            logoItem.title = nil
            logoItem.width = 0
            itemShowTable.image = nil
            itemShowTable.title = nil
            itemShowTable.width = 0
            searchField.width(constant: 600)
        }
    
        func showNaviLeft(){
    //        logoItem.image = UIImage(named: "logoMTSO_blue")
            logoItem.title = nil
            logoItem.width = 250
            itemShowTable.image = UIImage(named: "menu_close")
            itemShowTable.title = nil
    //        itemShowTable.width = 0
            logoItem.image = UIImage(named: "logoMTSO_blue")?.withRenderingMode(.alwaysOriginal)
            itemShowTable.width = 100
        }
    
        func next(){
            let vc = storyboard?.instantiateViewController(withIdentifier: "ThongKeVBDieuHanhController") as! ThongKeVBDieuHanhController
            navigationController?.pushViewController(vc,
                                                     animated: true)
        }
        func firtShowNavi(){
            if(SplitTableController.checkIsHiden){
                showNaviLeft()
            } else {
                hidenNaviLeft()
                
            }
        }
    
    
    @IBOutlet weak var viewBot: UIView!
    
    func setupViewHoan() {
        scrollView.bounces = false
        viewInfor.isHidden = true
        btnEditInfor.isHidden = true
        viewDisplayInfor.height(constant: 60)
        viewPassWord.height(constant: 60)
        viewSignature.height(constant: 60)
        viewSettingDefault.height(constant: 60)
        viewSoftwareSetting.height(constant: 60)
        viewChildSignature.isHidden = true
        viewChildSettingDefault.isHidden = true
        viewChildSoftwareSetting.isHidden = true
    //        viewDisplayInfor.layer.borderWidth = 1
        viewDisplayInfor.layer.cornerRadius = 25
        viewDisplayInfor.layer.shadowColor = UIColor.gray.cgColor
        viewDisplayInfor.layer.shadowOpacity = 2
        viewDisplayInfor.layer.shadowRadius = 2
        viewDisplayInfor.layer.shadowOffset = .zero
        
        viewPassWord.layer.cornerRadius = 25
        viewPassWord.layer.shadowColor = UIColor.gray.cgColor
        viewPassWord.layer.shadowOpacity = 2
        viewPassWord.layer.shadowRadius = 2
        viewPassWord.layer.shadowOffset = .zero
        
        viewSignature.layer.cornerRadius = 25
        viewSignature.layer.shadowColor = UIColor.gray.cgColor
        viewSignature.layer.shadowOpacity = 2
        viewSignature.layer.shadowRadius = 2
        viewSignature.layer.shadowOffset = .zero
        
        viewSettingDefault.layer.cornerRadius = 25
        viewSettingDefault.layer.shadowColor = UIColor.gray.cgColor
        viewSettingDefault.layer.shadowOpacity = 2
        viewSettingDefault.layer.shadowRadius = 2
        viewSettingDefault.layer.shadowOffset = .zero
        
        viewSoftwareSetting.layer.cornerRadius = 25
        viewSoftwareSetting.layer.shadowColor = UIColor.gray.cgColor
        viewSoftwareSetting.layer.shadowOpacity = 2
        viewSoftwareSetting.layer.shadowRadius = 2
        viewSoftwareSetting.layer.shadowOffset = .zero
    }
    
   
}

extension UIView {
  func height(constant: CGFloat) {
    setConstraint(value: constant, attribute: .height)
  }
  
  func width(constant: CGFloat) {
    setConstraint(value: constant, attribute: .width)
  }
  
    private func removeConstraint(attribute: NSLayoutConstraint.Attribute) {
    constraints.forEach {
      if $0.firstAttribute == attribute {
        removeConstraint($0)
      }
    }
  }
  
    private func setConstraint(value: CGFloat, attribute: NSLayoutConstraint.Attribute) {
    removeConstraint(attribute: attribute)
    let constraint =
      NSLayoutConstraint(item: self,
                         attribute: attribute,
                         relatedBy: NSLayoutConstraint.Relation.equal,
                         toItem: nil,
                         attribute: NSLayoutConstraint.Attribute.notAnAttribute,
                         multiplier: 1,
                         constant: value)
    self.addConstraint(constraint)
  }
}
