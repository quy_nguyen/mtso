//
//  NhiemVuChinhPhuGiao.swift
//  BGT
//
//  Created by vuquangnam on 9/2/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import Foundation
import UIKit

class NhiemVuChinhPhuGiaoController: UIViewController {

    @IBOutlet weak var navHeight: NSLayoutConstraint!
    @IBOutlet weak var tblContent: UITableView!
    @IBOutlet weak var navBotConstant: NSLayoutConstraint!
    @IBOutlet weak var btnNangCao: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeContent()
    }
    
    func initializeContent(){
        
        setupView()
    }
    
    func setupView(){
        self.view.layer.backgroundColor = Constant.Color.hex_F9FCFE.cgColor
        self.navHeight.constant = self.navigationBarHeight
        self.navBotConstant.constant = self.statusBarHeight/2
        
        self.btnNangCao.layer.borderWidth = 1
        self.btnNangCao.layer.borderColor = Constant.Color.border.cgColor
//        self.vYear.layer.cornerRadius = 15
//        self.vNumber.layer.cornerRadius = 15
//        self.btnSearch.layer.cornerRadius = 15
//        self.btnSearch.layer.borderWidth = 2
//        self.btnSearch.layer.borderColor = Constant.Color.hex_006DD2.cgColor
//
//        let VBDuThaoChoDuyetKyCell = UINib(nibName: "VBDuThaoChoDuyetKyCell", bundle: nil)
//        self.tblContent.register(VBDuThaoChoDuyetKyCell, forCellReuseIdentifier: "VBDuThaoChoDuyetKyCell")
//        self.tblContent.separatorStyle = .none
//        self.tblContent.delegate = self
//        self.tblContent.dataSource = self
    }

}
//extension NhiemVuChinhPhuGiaoController:UITableViewDataSource,UITableViewDelegate{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        
//    }
//
//
//}
