//
//  ChuyenVBController.swift
//  BGT
//
//  Created by Nguyen Hoan on 8/26/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

class ChuyenVBController: UIViewController {

    @IBOutlet weak var viewVB: UIView!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        viewVB.layer.borderWidth = 1
        
        viewVB.layer.borderColor = UIColor.gray.cgColor
        viewVB.backgroundColor = #colorLiteral(red: 0.9787469506, green: 0.9884774089, blue: 0.9968865514, alpha: 1)
        super.viewDidLoad()
        tableView.register(UINib(nibName: "ChuyenVanBanCell", bundle: nil), forCellReuseIdentifier: "ChuyenVanBanCell")
        tableView.register(UINib(nibName: "ChuyenVanBanTopCell", bundle: nil), forCellReuseIdentifier: "ChuyenVanBanTopCell")
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.layer.borderWidth = 1
        tableView.layer.borderColor = UIColor.gray.cgColor
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ChuyenVBController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0){
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ChuyenVanBanTopCell",for: indexPath) as? ChuyenVanBanTopCell {
                cell.backgroundColor = #colorLiteral(red: 0.7912601829, green: 0.8201960921, blue: 0.8457627892, alpha: 1)
                return cell
            }
        }
        else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ChuyenVanBanCell",for: indexPath) as? ChuyenVanBanCell {

                if(indexPath.row % 2 == 1){
                    cell.backgroundColor = #colorLiteral(red: 0.9646217227, green: 0.9647600055, blue: 0.9645913243, alpha: 1)
                }
                return cell
            }
        }
        
//                let rs = leftMenuText[indexPath.row]
//                cell.textField.text = rs
//                    cell.img.image = UIImage(named:"book")
//                cell.textField.text = leftMenuText[indexPath.row]
//                if(indexPath.row == 0){
////                    cell.layer.borderColor = UIColor.gray.cgColor
////                    cell.layer.borderWidth  = 2
//
//                }
//                      return cell
//            }
            return UITableViewCell()

        }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        check[indexPath.row] = !check[indexPath.row]
//        leftMenuText = Constant.LeftMenuText.resultForLeftMenu(check: check)
//        leftMenuTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

}
