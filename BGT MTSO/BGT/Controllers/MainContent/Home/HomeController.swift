//
//  HomeController.swift
//  BGT
//
//  Created by Nguyen Hoan on 8/25/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit
import ScrollableDatepicker

class HomeController: UIViewController {
    
    @IBOutlet weak var navBotConstant: NSLayoutConstraint!
    @IBOutlet weak var navHeight: NSLayoutConstraint!
    @IBOutlet weak var vHeader: UIView!
    @IBOutlet weak var lblTitle: NSLayoutConstraint!
    @IBOutlet weak var lblDateFrom: UILabel!
    @IBOutlet weak var lblDateTo: UILabel!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var vNameDropDown: UIView!
    @IBOutlet weak var vDateFrom: UIView!
    @IBOutlet weak var vDateTo: UIView!
    @IBOutlet weak var tblCalendarContent: UITableView!
    @IBOutlet weak var vDatePicker: ScrollableDatepicker!{
        didSet {
           var dates = [Date]()
           for day in -2...28 {
               dates.append(Date(timeIntervalSinceNow: Double(day * 86400)))
           }

           vDatePicker.dates = dates
           vDatePicker.selectedDate = Date()
           vDatePicker.delegate = self

            var configuration = Configuration()
            configuration.defaultDayStyle.dateTextColor = Constant.Color.hex_053482
            configuration.defaultDayStyle.dateTextFont = .systemFont(ofSize: 20, weight: UIFont.Weight.medium)
            configuration.defaultDayStyle.weekDayTextColor = Constant.Color.hex_053482
            configuration.defaultDayStyle.weekDayTextFont = .systemFont(ofSize: 16, weight: UIFont.Weight.medium)
            configuration.defaultDayStyle.backgroundColor = Constant.Color.hex_E1EDFF
            configuration.weekendDayStyle.weekDayTextFont = .systemFont(ofSize: 16, weight: UIFont.Weight.medium)
            
            configuration.selectedDayStyle.dateTextColor = .white
            configuration.selectedDayStyle.weekDayTextColor = .white
            configuration.selectedDayStyle.backgroundColor = Constant.Color.hex_1352BA
            configuration.daySizeCalculation = .numberOfVisibleItems(7)

            vDatePicker.configuration = configuration
       }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeContent()
    }
    
    func initializeContent(){
        
        setupView()
    }
    
    func setupView(){
        presentLoginView()
        
        self.view.layer.backgroundColor = Constant.Color.hex_F9FCFE.cgColor
        
        self.navHeight.constant = self.navigationBarHeight
        self.navBotConstant.constant = self.statusBarHeight/2
        
        self.vNameDropDown.layer.cornerRadius = 15
        self.vDateTo.layer.cornerRadius = 15
        self.vDateFrom.layer.cornerRadius = 15
        self.btnSearch.layer.cornerRadius = 15
        self.btnSearch.layer.borderWidth = 2
        self.btnSearch.layer.borderColor = Constant.Color.hex_006DD2.cgColor
        self.vHeader.layer.borderWidth = 1
        self.vHeader.layer.borderColor = Constant.Color.border.cgColor
        let workingScheduleCell = UINib(nibName: "WorkingScheduleCell", bundle: nil)
        self.tblCalendarContent.register(workingScheduleCell, forCellReuseIdentifier: "WorkingScheduleCell")
        self.tblCalendarContent.separatorStyle = .none
        self.tblCalendarContent.delegate = self
        self.tblCalendarContent.dataSource = self
    }
    func presentLoginView(){
        let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = mainstoryboard.instantiateViewController(withIdentifier: "LoginController") as? LoginController else { return }
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: false, completion: nil)
    }
}


extension HomeController: ScrollableDatepickerDelegate {

    func datepicker(_ datepicker: ScrollableDatepicker, didSelectDate date: Date) {
        
    }
    
}

extension HomeController: UITableViewDataSource,UITableViewDelegate{
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"WorkingScheduleCell", for: indexPath) as! WorkingScheduleCell
        cell.selectionStyle = .none
       
        
        return cell

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 495
    }

}
