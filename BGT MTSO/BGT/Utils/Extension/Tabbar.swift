//
//  Tabbar.swift
//  BGT
//
//  Created by Macbook Pro 2017 on 8/31/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit
class TabBar: UITabBar {

    override var traitCollection: UITraitCollection {
        guard UIDevice.current.userInterfaceIdiom == .pad else {
            return super.traitCollection
        }

        return UITraitCollection(horizontalSizeClass: .compact)
    }
}
