//
//  ViewController+Extension.swift
//  BGT
//
//  Created by Macbook Pro 2017 on 8/31/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit
extension UIViewController {
    var navigationBarHeight: CGFloat {
        return self.navigationController?.navigationBar.frame.height ?? 0.0
    }
    var statusBarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.size.height
    }
}
