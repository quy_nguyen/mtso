//
//  Endcodable+Extension.swift
//  BGT
//
//  Created by Macbook Pro 2017 on 9/1/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import Foundation
extension Encodable {
    func setParams() -> [String:Any] {
        var params: [String: Any] = [:]
        let data = try! JSONEncoder().encode(self)
        params = try! JSONSerialization.jsonObject(with: data, options: []) as! [String : Any]
        return params
    }
}
