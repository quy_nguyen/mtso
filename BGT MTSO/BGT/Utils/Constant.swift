//
//  Constant.swift
//  OneHome
//
//  Created by Macbook Pro 2017 on 7/3/20.
//  Copyright © 2020 FDS. All rights reserved.
//

import UIKit


struct Constant {
    
    struct Color {
        static let hex_053482 = UIColor(red: 5/255, green: 52/255, blue: 130/255, alpha: 1)
        static let hex_053482_opa_80 = UIColor(red: 5/255, green: 52/255, blue: 130/255, alpha: 0.8)
        static let hex_006DD2 = UIColor(red: 0/255, green: 109/255, blue: 210/255, alpha: 1)
        static let hex_E1EDFF = UIColor(red: 225/255, green: 237/255, blue: 255/255, alpha: 1)
        static let hex_1352BA = UIColor(red: 19/255, green: 83/255, blue: 186/255, alpha: 1)
        static let hex_E9F1F8 = UIColor(red: 233/255, green: 241/255, blue: 248/255, alpha: 1)
        static let hex_1D1D1D = UIColor(red: 29/255, green: 29/255, blue: 29/255, alpha: 1)
        static let hex_46A5FC = UIColor(red: 70/255, green: 165/255, blue: 252/255, alpha: 1)
        static let hex_F9FCFE = UIColor(red: 249/255, green: 252/255, blue: 254/255, alpha: 1)
        static let hex_4FA0CA = UIColor(red: 79/255, green: 160/255, blue: 202/255, alpha: 1)
        static let hex_6E77FD = UIColor(red: 110/255, green: 119/255, blue: 253/255, alpha: 1)
        static let hex_727383 = UIColor(red: 114/255, green: 115/255, blue: 131/255, alpha: 1)
        static let hex_DAAF9E = UIColor(red: 218/255, green: 175/255, blue: 158/255, alpha: 1)
        static let hex_FD614A = UIColor(red: 253/255, green: 97/255, blue: 74/255, alpha: 1)
        static let hex_FEA621 = UIColor(red: 254/255, green: 166/255, blue: 33/255, alpha: 1)
        static let border = UIColor(red: 227/255, green:231/255, blue: 234/255, alpha: 1)
        static let blue_dim = UIColor(red: 46/255, green: 108/255, blue: 203/255, alpha: 1)
    }

    
    struct  LeftMenuText {
        static let hightRoot = 50
        static let hightChild = 40
        static let hightChildMin = 30
        static var countTotal = root.count + child_1.count + child_1_0.count
        static let root = ["Lịch công tác","Văn bản điều hành","Nhiệm vụ chính phủ giao"]
//        static let child_0 = []
        static let child_1 = ["Văn bản chưa xử lý","Văn bản dự thảo chờ phê duyệt/ký"]
        static let child_1_0 = ["Văn bản từ chính phủ","Văn bản tử Bộ GTVT","Giấy mời","Văn bản từ Bộ/UBND","Văn bản từ Sở/Huyện", "Văn bản khác"]
//        static let child_2 = []
        static let allText = getAllText()
        static func getAllText() -> [String] {
            var rs = [String]()
            rs.append(root[0])
            rs.append(root[1])
            rs += child_1
            rs += child_1_0
                        rs.append(root[2])
            return rs
        }
        static func resultForLeftMenu(check: [Bool])  -> [String]{
            var rs = [String]()
            rs.append(root[0])
            rs.append(root[1])
            
            if check[1] {
                rs.append(child_1[0])
                if check[2]{
                    rs+=child_1_0
                }
                rs.append(child_1[1])
            }
            rs.append(root[2])
            
            return rs
        }
        static func checkRoot(str:String) -> Bool {
                
                   return root.contains(str)
           }
        static func checkChild1(str: String) ->Bool {
            return child_1.contains(str)
        }
        
    }
        
    struct Router {
        static let fdsDomain = "https://issues.fds.vn/"
        static let bgtDomain = "http://sso-bgtvt.fds.vn/"

        // Login
        static let login = "auth/realms/sso-demo/protocol/openid-connect/token"
        static let userDetail = "vuejx/ext/UserDetail"

        // User Info
        static let canBoDetail = "vuejx/ext/CanBoDetail"
        static let T_Canbo = "security/upload/T_CanBo"
        static let capNhatCanBoDetail = "vuejx/ext/CapNhatCanBoDetail"
        static let changepass = "keycloak-service/api/users/changepass"

        // Văn bản + dự thảo dữ liệu
        
        static let nhomVanBanDen = "vuejx/ext/DMVanBan"
        static let trangThaiDuThao = "vuejx/ext/TrangThaiDuThao"
        static let VBChuaXuLy = "vuejx/ext/VBChuaXuLy"
        static let chiTietVBChuaXuLy = "vuejx/ext/ChiTietVBChuaXuLy"
        static let duThaoChoDuyetKy = "vuejx/ext/DuThaoChoDuyetKy"
        static let chiTietDuThao = "vuejx/ext/ChiTietDuThao"

        // Văn bản + dự thảo actions
        static let xuLyXong = "vuejx/ext/VanBanXuLyXong"
        static let chuyenVBDen = "vuejx/ext/ChuyenVBDen"
        static let duThaoTraLai = "vuejx/ext/DuThaoTraLai"
        static let duThaoKyPH = "vuejx/ext/DuThaoKyPH"
        static let duThaoKyPHDuyet = "vuejx/ext/DuThaoKyPHDuyet"

        //Thống kê
        static let thongKeDuThaoChoDuyetKy = "vuejx/ext/ThongKeDuThaoChoDuyetKy"
        static let thongKeVanBanChuaXuLy = "vuejx/ext/ThongKeVanBanChuaXuLy"
    }
}

