//
//  OfficialsInfoResponse.swift
//  BGT
//
//  Created by Quý Nguyễn on 9/2/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import Foundation
import UIKit
struct OfficialsInfoResponse : Codable {
    let status : Int?
    let message : String?
    let uri : String?
    let total : totalResponse?
    let data : [dataResponse]?

    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
        case uri = "uri"
        case total = "total"
        case data = "data"

    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        uri = try values.decodeIfPresent(String.self, forKey: .uri)
        total = try values.decodeIfPresent(totalResponse.self, forKey: .total)
        data = try values.decodeIfPresent([dataResponse].self, forKey: .data)
    }
    
}

struct totalResponse : Codable {
    let value : Int?
    let relation : String?
    
    enum CodingKeys: String, CodingKey {
        case value = "value"
        case relation = "relation"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        value = try values.decodeIfPresent(Int.self, forKey: .value)
        relation = try values.decodeIfPresent(String.self, forKey: .relation)
    }
    
}

struct dataResponse : Codable {
    let _index : String?
    let _type : String?
    let _id : String?
    let _score : Float?
    let _source : sourceResponse?
    
    enum CodingKeys: String, CodingKey {
        case _index = "_index"
        case _type = "_type"
        case _id = "_id"
        case _score = "_score"
        case _source = "_source"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _index = try values.decodeIfPresent(String.self, forKey: ._index)
        _type = try values.decodeIfPresent(String.self, forKey: ._type)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        _score = try values.decodeIfPresent(Float.self, forKey: ._score)
        _source = try values.decodeIfPresent(sourceResponse.self, forKey: ._source)
    }
}

struct sourceResponse : Codable {
    let id : String?
    let type : String?
    let site : String?
    let order : Int?
    let HoVaTen : String?
    let storage : String?
    let username : String?
    let createdAt : Int?
    let openAccess : Int?
    let modifiedAt : Int?
    let SoHieuCanBo : String?
    let lastusername : String?
    let ViTriChucVu : [ViTriChucVuResponse]?
    let accessRoles : [accessRolesResponse]?
    let APPPhanMemNV : [APPPhanMemNVResponse]?
    let CoQuanCongTac : [CoQuanCongTacResponse]?
    let TaiKhoanDienTu : TaiKhoanDienTuResponse?
    let TinhTrangCongTac : TinhTrangCongTacResponse?
    let shortName : String?
    let createdAt___year : Int?
    let createdAt___month : Int?
    let modifiedAt___year : Int?
    let modifiedAt___month : Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case site = "site"
        case order = "order"
        case HoVaTen = "HoVaTen"
        case storage = "storage"
        case username = "username"
        case createdAt = "createdAt"
        case openAccess = "openAccess"
        case modifiedAt = "modifiedAt"
        case SoHieuCanBo = "SoHieuCanBo"
        case lastusername = "lastusername"
        case ViTriChucVu = "ViTriChucVu"
        case accessRoles = "accessRoles"
        case APPPhanMemNV = "APPPhanMemNV"
        case CoQuanCongTac = "CoQuanCongTac"
        case TaiKhoanDienTu = "TaiKhoanDienTu"
        case TinhTrangCongTac = "TinhTrangCongTac"
        case shortName = "shortName"
        case createdAt___year = "createdAt___year"
        case createdAt___month = "createdAt___month"
        case modifiedAt___year = "modifiedAt___year"
        case modifiedAt___month = "modifiedAt___month"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        site = try values.decodeIfPresent(String.self, forKey: .site)
        order = try values.decodeIfPresent(Int.self, forKey: .order)
        HoVaTen = try values.decodeIfPresent(String.self, forKey: .HoVaTen)
        storage = try values.decodeIfPresent(String.self, forKey: .storage)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        createdAt = try values.decodeIfPresent(Int.self, forKey: .createdAt)
        openAccess = try values.decodeIfPresent(Int.self, forKey: .openAccess)
        modifiedAt = try values.decodeIfPresent(Int.self, forKey: .modifiedAt)
        SoHieuCanBo = try values.decodeIfPresent(String.self, forKey: .SoHieuCanBo)
        lastusername = try values.decodeIfPresent(String.self, forKey: .lastusername)
        ViTriChucVu = try values.decodeIfPresent([ViTriChucVuResponse].self, forKey: .ViTriChucVu)
        accessRoles = try values.decodeIfPresent([accessRolesResponse].self, forKey: .accessRoles)
        APPPhanMemNV = try values.decodeIfPresent([APPPhanMemNVResponse].self, forKey: .APPPhanMemNV)
        CoQuanCongTac = try values.decodeIfPresent([CoQuanCongTacResponse].self, forKey: .CoQuanCongTac)
        TaiKhoanDienTu = try values.decodeIfPresent(TaiKhoanDienTuResponse.self, forKey: .TaiKhoanDienTu)
        TinhTrangCongTac = try values.decodeIfPresent(TinhTrangCongTacResponse.self, forKey: .TinhTrangCongTac)
        shortName = try values.decodeIfPresent(String.self, forKey: .shortName)
        createdAt___year = try values.decodeIfPresent(Int.self, forKey: .createdAt___year)
        createdAt___month = try values.decodeIfPresent(Int.self, forKey: .createdAt___month)
        modifiedAt___year = try values.decodeIfPresent(Int.self, forKey: .modifiedAt___year)
        modifiedAt___month = try values.decodeIfPresent(Int.self, forKey: .modifiedAt___month)
    }
}

struct ViTriChucVuResponse : Codable {
    let _index : String?
    let _type : String?
    let _id : String?
    let _score : Float?
    
    enum CodingKeys: String, CodingKey {
        case _index = "_index"
        case _type = "_type"
        case _id = "_id"
        case _score = "_score"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _index = try values.decodeIfPresent(String.self, forKey: ._index)
        _type = try values.decodeIfPresent(String.self, forKey: ._type)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        _score = try values.decodeIfPresent(Float.self, forKey: ._score)
    }
    
}

struct accessRolesResponse : Codable {
    let _index : String?
    let _type : String?
    let _id : String?
    let _score : Float?
    
    enum CodingKeys: String, CodingKey {
        case _index = "_index"
        case _type = "_type"
        case _id = "_id"
        case _score = "_score"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _index = try values.decodeIfPresent(String.self, forKey: ._index)
        _type = try values.decodeIfPresent(String.self, forKey: ._type)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        _score = try values.decodeIfPresent(Float.self, forKey: ._score)
    }
}

struct APPPhanMemNVResponse : Codable {
    let _index : String?
    let _type : String?
    let _id : String?
    let _score : Float?
    
    enum CodingKeys: String, CodingKey {
        case _index = "_index"
        case _type = "_type"
        case _id = "_id"
        case _score = "_score"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _index = try values.decodeIfPresent(String.self, forKey: ._index)
        _type = try values.decodeIfPresent(String.self, forKey: ._type)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        _score = try values.decodeIfPresent(Float.self, forKey: ._score)
    }
}

struct CoQuanCongTacResponse : Codable {
    let _index : String?
    let _type : String?
    let _id : String?
    let _score : Float?
    
    enum CodingKeys: String, CodingKey {
        case _index = "_index"
        case _type = "_type"
        case _id = "_id"
        case _score = "_score"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _index = try values.decodeIfPresent(String.self, forKey: ._index)
        _type = try values.decodeIfPresent(String.self, forKey: ._type)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        _score = try values.decodeIfPresent(Float.self, forKey: ._score)
    }
}

struct TaiKhoanDienTuResponse : Codable {
   let _index : String?
    let _type : String?
    let _id : String?
    let _score : Float?
    
    enum CodingKeys: String, CodingKey {
        case _index = "_index"
        case _type = "_type"
        case _id = "_id"
        case _score = "_score"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _index = try values.decodeIfPresent(String.self, forKey: ._index)
        _type = try values.decodeIfPresent(String.self, forKey: ._type)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        _score = try values.decodeIfPresent(Float.self, forKey: ._score)
    }
}

struct TinhTrangCongTacResponse : Codable {
    let _index : String?
    let _type : String?
    let _id : String?
    let _score : Float?
    
    enum CodingKeys: String, CodingKey {
        case _index = "_index"
        case _type = "_type"
        case _id = "_id"
        case _score = "_score"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _index = try values.decodeIfPresent(String.self, forKey: ._index)
        _type = try values.decodeIfPresent(String.self, forKey: ._type)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        _score = try values.decodeIfPresent(Float.self, forKey: ._score)
    }
}
