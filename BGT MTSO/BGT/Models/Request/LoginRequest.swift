//
//  LoginRequest.swift
//  BGT
//
//  Created by Macbook Pro 2017 on 9/1/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import Foundation

struct LoginRequest: Codable {
    var client_id: String?
    var client_secret: String?
    var username: String?
    var password: String?
    var grant_type: String?
}
