//
//  ViewController.swift
//  BGT
//
//  Created by HoanMata on 8/6/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var btn_checkBox: UIButton!

    @IBAction func checkBoxTapped(_ sender: UIButton) {
        if sender.isSelected{
            sender.isSelected = false
        } else{
            sender.isSelected = true
        }
        
    }
    @IBOutlet weak var checkBoxTapped: UIButton!
    
    @IBOutlet weak var textField_username: UITextField!
    @IBOutlet weak var textField_password: UITextField!
    @IBOutlet weak var btn_Login: UIButton!
    @IBOutlet weak var btn_LoginFinger: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        btn_LoginFinger.backgroundColor = UIColor.clear
        btn_LoginFinger.layer.cornerRadius = 4
        btn_LoginFinger.layer.borderWidth = 1
        btn_LoginFinger.layer.borderColor = UIColor.white.cgColor
        btn_Login.layer.cornerRadius = 4
        // Do any additional setup after loading the view.
        let imgUsername:UIImage! = UIImage(named: "account")
        let imgPassword:UIImage! = UIImage(named: "lock")
        addLeftImageTo(txtField: textField_username, andImage: imgUsername)
        addLeftImageTo(txtField: textField_password, andImage: imgPassword)
        
    }
    func addLeftImageTo(txtField: UITextField, andImage img: UIImage) {
        let leftImageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: img.size.width, height: img.size.height))
        leftImageView.image = img
        txtField.leftView = leftImageView
        txtField.leftViewMode = .always
    }
    
}

