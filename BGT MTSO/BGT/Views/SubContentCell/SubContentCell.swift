//
//  SubContentCell.swift
//  BGT
//
//  Created by vuquangnam on 9/1/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

class SubContentCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vBorder: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTimeNumber: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblPrepare: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupView(){
        contentView.layer.masksToBounds = true
        self.lblTitle.textColor = Constant.Color.hex_053482
        self.lblTime.textColor = Constant.Color.hex_006DD2
        self.lblTimeNumber.textColor = Constant.Color.hex_053482
        self.lblLocation.textColor = Constant.Color.hex_006DD2
    }
    
}
