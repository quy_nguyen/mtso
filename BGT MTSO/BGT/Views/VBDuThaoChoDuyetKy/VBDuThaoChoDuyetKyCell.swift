//
//  VBDuThaoChoDuyetKyCell.swift
//  BGT
//
//  Created by vuquangnam on 9/1/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

class VBDuThaoChoDuyetKyCell: UITableViewCell {

    @IBOutlet weak var vSpacing: UIView!
    @IBOutlet weak var vContent: UIView!
    @IBOutlet weak var vBorder: UIView!
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblAuthorOrgan: UILabel!
    
   override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupView(){
        self.lblID.textColor = Constant.Color.hex_053482
        self.lblContent.textColor = Constant.Color.hex_053482
        self.vContent.layer.borderWidth = 1
        self.vContent.layer.borderColor = Constant.Color.border.cgColor
        self.vBorder.layer.backgroundColor = Constant.Color.hex_46A5FC.cgColor
    }
    
}
