//
//  ResultFilterCell.swift
//  BGT
//
//  Created by HoanMata on 8/10/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

class ResultFilterCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layer.borderColor = #colorLiteral(red: 0.8916916251, green: 0.906211257, blue: 0.9189206958, alpha: 1)
        contentView.layer.borderWidth = 1 
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0))
    }
    
}
