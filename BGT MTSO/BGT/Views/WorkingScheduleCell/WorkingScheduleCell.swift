//
//  WorkingSchedule.swift
//  BGT
//
//  Created by vuquangnam on 8/31/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

class WorkingScheduleCell: UITableViewCell {

    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var vName: UIView!
    @IBOutlet weak var vSpacing: UIView!
    @IBOutlet weak var tblContent: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setupView(){
        self.vName.backgroundColor = Constant.Color.hex_053482
        
        let SubContentCell = UINib(nibName: "SubContentCell", bundle: nil)
        self.tblContent.register(SubContentCell, forCellReuseIdentifier: "SubContentCell")
        self.tblContent.separatorStyle = .none
        self.tblContent.delegate = self
        self.tblContent.dataSource = self
    }
}

extension WorkingScheduleCell:  UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"SubContentCell", for: indexPath) as! SubContentCell
        cell.selectionStyle = .none
       
        
        return cell

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 160
    }
    
    
}
