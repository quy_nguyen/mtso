//
//  ChuyenVanBanCell.swift
//  BGT
//
//  Created by Nguyen Hoan on 8/19/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit
protocol ChuyenVanBanCellDelegate {
    func  reloadDataTable()
}

class ChuyenVanBanCell: UITableViewCell {
    var indexCell: Int = 0
//    var delegate: ChuyenVanBanCellDelegate?
    @IBOutlet weak var btn_parent: UIButton!
    @IBOutlet weak var btn_ChuTri: RadioButton!
    @IBOutlet weak var textlb: UILabel!
    @IBOutlet weak var btn_TheoDoi: Checkbox!
    @IBOutlet weak var btn_PhoiHop: Checkbox!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
////        btn_ChuTri.borderCornerRadius = btn_ChuTri.frame.height/2
////        btn_ChuTri.borderStyle = .circle
////        btn_ChuTri.checkboxFillColor = UIColor.white
////        btn_ChuTri.checkmarkStyle = .circle
////        btn_PhoiHop.borderStyle = .square
////        btn_PhoiHop.checkboxFillColor = UIColor.white
//        print("select")
//
////
////        btn_PhoiHop.checkmarkStyle = .tick
//        // Configure the view for the selected state
//    }

//    @IBAction func btn_parentTapped(_ sender: Any) {
//        delegate?.reloadDataTable()
//    }
//
//    @IBAction func btn_theoDoiTapped(_ sender: Any) {
//
//    }
//    @IBAction func btn_chuTriTapped
//        (_ sender: Any) {
//
//    }
//    @IBAction func btn_chuPhoiHopTapped
//           (_ sender: Any) {
//
//       }
}

