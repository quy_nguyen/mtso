//
//  InforCell.swift
//  BGT
//
//  Created by HoanMata on 8/10/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

class InforCell: UITableViewCell {
    @IBOutlet weak var nameDisplay: UILabel!
    
    @IBOutlet weak var displayNameLabel: UILabel!
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var imageInfor: UIImageView!
    @IBOutlet weak var fullnameLable: UILabel!
    @IBOutlet weak var room: UILabel!
    @IBOutlet weak var roomLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var position: UILabel!
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var email: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }


}
