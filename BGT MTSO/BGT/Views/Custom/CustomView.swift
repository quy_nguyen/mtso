import  Foundation
import  UIKit
class CustomView: UIView {
    let margin = CGFloat(16)
    
    let backView: UIView = {
       let view = UIView()
        view.backgroundColor = UIColor(red: 242/250, green: 242/250, blue: 242/250, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
   
    let titleLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .bold)
        label.text = "Họp về quản lý hoạt động của các trường trực bộ"
        label.numberOfLines = 0
        label.lineBreakMode = .byClipping
        label.textAlignment = .left
        label.textColor = UIColor(red: 0/250, green: 0/250, blue: 102/250, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let timeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.text = "Thời gian:"
        label.textAlignment = .left
        label.textColor = UIColor(red: 0/250, green: 102/250, blue: 235/250, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let timeNumber: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
        label.text = "14:00"
        label.textAlignment = .left
        label.textColor = UIColor(red: 0/250, green: 51/250, blue: 204/250, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let line: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 0/250, green: 102/250, blue: 245/250, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let locationLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.text = "Địa điểm:"
        label.textAlignment = .left
        label.textColor = UIColor(red: 0/250, green: 102/250, blue: 235/250, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let locationInput: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.text = "Hội trường 2D"
        label.textAlignment = .left
        label.textColor = UIColor(red: 0/250, green: 102/250, blue: 235/250, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let prepareLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.numberOfLines = 0
        label.lineBreakMode = .byClipping
        label.text = "Chuẩn bị:"
        label.textAlignment = .left
        label.textColor = UIColor.black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let participantsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.numberOfLines = 0
        label.lineBreakMode = .byClipping
        label.text = "Thành phần tham gia:"
        label.textAlignment = .left
        label.textColor = UIColor.black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        setupLayout()
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLayout() {
        self.addSubview(backView)
        NSLayoutConstraint.activate([
            backView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            backView.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 0),
            backView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0)
        ])
        
        backView.addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: backView.topAnchor, constant: margin),
            titleLabel.leadingAnchor.constraint(equalTo: backView.leadingAnchor,constant: margin),
            titleLabel.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: -margin),
        ])
        
        backView.addSubview(timeLabel)
        NSLayoutConstraint.activate([
            timeLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5),
            timeLabel.leadingAnchor.constraint(equalTo: backView.leadingAnchor,constant: margin),
        ])
        
        backView.addSubview(timeNumber)
        NSLayoutConstraint.activate([
            timeNumber.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5),
            timeNumber.leadingAnchor.constraint(equalTo: timeLabel.trailingAnchor,constant: 4)
        ])
        
        backView.addSubview(line)
        NSLayoutConstraint.activate([
            line.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5),
            line.leadingAnchor.constraint(equalTo: timeNumber.trailingAnchor,constant: 20),
            line.widthAnchor.constraint(equalToConstant: 1),
            line.heightAnchor.constraint(equalToConstant: 14)
        ])
        
        backView.addSubview(locationLabel)
        NSLayoutConstraint.activate([
            locationLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5),
            locationLabel.leadingAnchor.constraint(equalTo: line.trailingAnchor,constant: 20),
        ])
        
        backView.addSubview(locationInput)
        NSLayoutConstraint.activate([
            locationInput.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5),
            locationInput.leadingAnchor.constraint(equalTo: locationLabel.trailingAnchor,constant: 4),
        ])
        
        backView.addSubview(prepareLabel)
        NSLayoutConstraint.activate([
            prepareLabel.topAnchor.constraint(equalTo: timeLabel.bottomAnchor, constant: 6),
            prepareLabel.leadingAnchor.constraint(equalTo: backView.leadingAnchor,constant: margin),
            prepareLabel.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: -margin)
        ])
        
        backView.addSubview(participantsLabel)
        NSLayoutConstraint.activate([
            participantsLabel.topAnchor.constraint(equalTo: prepareLabel.bottomAnchor, constant: 6),
            participantsLabel.leadingAnchor.constraint(equalTo: backView.leadingAnchor,constant: margin),
            participantsLabel.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: -margin)
        ])
        
        
        //SetnewConstraint for Backview
        var bottomConstraint: NSLayoutConstraint?
        bottomConstraint = backView.bottomAnchor.constraint(equalTo: participantsLabel.bottomAnchor, constant: margin)
        bottomConstraint?.isActive = true
    }
    
}
