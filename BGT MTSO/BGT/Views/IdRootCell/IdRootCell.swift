//
//  IdRootCell.swift
//  BGT
//
//  Created by HoanMata on 8/12/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

class IdRootCell: UITableViewCell {

    @IBOutlet weak var btn_arrow: UIButton!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var textField: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        

        // Configure the view for the selected state
    }

}
