//
//  VBChuaXuLyCell.swift
//  BGT
//
//  Created by vuquangnam on 9/1/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import UIKit

class VBChuaXuLyCell: UITableViewCell {

    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var lblNumberCode: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblExpire: UILabel!
    @IBOutlet weak var vContent: UIView!
    @IBOutlet weak var vSpacing: UIView!
    @IBOutlet weak var vExpire: UIView!
    @IBOutlet weak var vBorder: UIView!
    @IBOutlet weak var lblOrganization: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupView(){
        self.lblID.textColor = Constant.Color.hex_053482
        self.lblNumberCode.textColor = Constant.Color.hex_006DD2
        self.lblExpire.textColor = Constant.Color.hex_006DD2
        self.lblOrganization.textColor = Constant.Color.hex_1D1D1D
        self.lblContent.textColor = Constant.Color.hex_053482
        self.vExpire.layer.backgroundColor = Constant.Color.hex_E9F1F8.cgColor
        self.vBorder.layer.backgroundColor = Constant.Color.hex_46A5FC.cgColor
    }
    
}
