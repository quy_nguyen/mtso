//
//  TableViewCell.swift
//  NotificationStoryBoard
//
//  Created by Truong on 8/25/20.
//  Copyright © 2020 Truong. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var topLine: UIView!
    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var line: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var hasLabel: UILabel!
    @IBOutlet weak var numberOfDocumentLabel: UILabel!
    @IBOutlet weak var documentLabel: UILabel!
    @IBOutlet weak var documentStatusLabel: UILabel!
    @IBOutlet weak var seenLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        //setImageForLeftImage
        leftImage.image = UIImage(named: "image1")
        
        //seenLabel
        seenLabel.font = UIFont(name: "Arial-BoldItalicMT", size: 18)
    }
    
}
