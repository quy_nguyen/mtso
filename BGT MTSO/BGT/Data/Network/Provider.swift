//
//  Provider.swift
//  BGT
//
//  Created by Macbook Pro 2017 on 9/1/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


typealias RequestCompletion = ((_ success: Bool, _ IsFailResponseError: Bool, _ data: Any?) -> (Void))?

class Provider {
    var alamofireManager: Alamofire.Session?
    
    fileprivate var request: Request?

    fileprivate let networkTimeout: TimeInterval = 30.0
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = networkTimeout
        configuration.timeoutIntervalForResource = networkTimeout
        alamofireManager = Alamofire.Session(configuration: configuration)
    }
    
    fileprivate func getDefaultHeaderTypeJSON(api: ClientApi) -> HTTPHeaders {
        var headers = HTTPHeaders()
        
        if api == ClientApi.login || api == ClientApi.changepass {
            headers["Content-Type"] = "application/x-www-form-urlencoded"
        }
        if let accessToken = UserDefaultUtils.shared.getAccessToken() {
            headers["Authorization"] = "Bearer " + accessToken
    //            headers["X-ApiToken"] = accessToken
        }
    //        headers["Accept"] = "application/vnd.api+json"
            
        return headers
    }
    
    func cancel() {
        request?.cancel()
    }
}

extension Provider {
    func requestAPIJSON(api: ClientApi, parameters: [String : Any], headers: HTTPHeaders? = nil, encoding: ParameterEncoding? = nil, completion: RequestCompletion) {
        let url = api.path

        let finalHeaders: HTTPHeaders = {
            if let headers = headers {
                return headers
            }
            return getDefaultHeaderTypeJSON(api: api)
        }()
        let finalEncoding: ParameterEncoding = {
            if let encoding = encoding {
                return encoding
            }
            return JSONEncoding.prettyPrinted
        }()
        
        alamofireManager?.request(url, method: api.method, parameters: parameters, encoding: finalEncoding, headers: finalHeaders).cURLDescription(calling: { (curl) in
            print(curl)
        }) .responseJSON(completionHandler: { (response) in
            let statusCode = response.response?.statusCode
            print("---------------------------------")
            print(statusCode)
            print(response)
            switch response.result {
            case .success(let data):
                switch statusCode {
                case 200:
                    completion?(true, false, data)
                    
                case 400:
                    completion?(false, false, data)

                default:
                    completion?(false, false, data)

                    print("Kết nối thất bại")
                }
            case .failure( _):
                completion?(false, true, nil)
            }
        })
    }
    
    
    func GetRequestAPIJSON(api: ClientApi, subPathAPI: String? = nil, parameters: [String : Any]? = [:], headers: HTTPHeaders? = nil, encoding: ParameterEncoding? = nil, completion: RequestCompletion) {
        let url: String = {
            if let subPathAPI = subPathAPI, subPathAPI != "" {
                return api.path + "/\(subPathAPI)"
            } else {
                return api.path
            }
        }()
        
        let finalHeaders: HTTPHeaders = {
            if let headers = headers {
                return headers
            }
            return getDefaultHeaderTypeJSON(api: api)
        }()
        let finalEncoding: ParameterEncoding = {
            if let encoding = encoding {
                return encoding
            }
            return URLEncoding.default
        }()
        
        alamofireManager?.request(url, method: api.method, parameters: parameters, encoding: finalEncoding, headers: finalHeaders).cURLDescription(calling: { (curl) in
            print(curl)
        }) .responseJSON(completionHandler: { (response) in
            let statusCode = response.response?.statusCode

            switch response.result {
            case .success(let data):
                switch statusCode {
                case 200:
                    completion?(true, false, data)

                case 400:
                    completion?(false, false, data)

                default:
                    print("Kết nối thất bại")
                    completion?(false, false, nil)
                }
            case .failure( _):
                completion?(false, true, nil)
            }
        })
    }
}

