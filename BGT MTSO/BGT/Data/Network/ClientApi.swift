//
//  ClientApi.swift
//  BGT
//
//  Created by Macbook Pro 2017 on 9/1/20.
//  Copyright © 2020 HoanMata. All rights reserved.
//

import Foundation
import Alamofire

enum ClientApi {
    
    // Login
    case login
    case userDetail

    // User Info
    case canBoDetail
    case T_Canbo
    case capNhatCanBoDetail
    case changepass
    case layDanhSachFileDinhKem
    
    // Văn bản + dự thảo dữ liệu
    case DMVanBan
    case trangThaiDuThao
    case VBChuaXuLy
    case chiTietVBChuaXuLy
    case duThaoChoDuyetKy
    case chiTietDuThao
    
    // Văn bản + dự thảo actions
    case xuLyXong
    case chuyenVBDen
    case duThaoTraLai
    case duThaoKyPH
    case duThaoKyPHDuyet

    //Thống kê
    case thongKeDuThaoChoDuyetKy
    case thongKeVanBanChuaXuLy
    
}

extension ClientApi: TargetType {
    var baseBgtURL: String {
        return Constant.Router.bgtDomain
    }

    /// The target's base `URL`.
    var baseFdsURL: String {
        return Constant.Router.fdsDomain
    }
    
    
    
    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String {
        switch self {
            // Login
        case .login:
            return baseBgtURL + Constant.Router.login
        case .userDetail:
            return baseBgtURL + Constant.Router.login
            
        // User Info
        case .canBoDetail:
            return baseBgtURL + Constant.Router.login
        case .T_Canbo:
            return baseBgtURL + Constant.Router.login
        case .capNhatCanBoDetail:
            return baseBgtURL + Constant.Router.login
        case .changepass:
            return baseBgtURL + Constant.Router.login

            // Văn bản + dự thảo dữ liệu
        case .layDanhSachFileDinhKem:
            return baseBgtURL + Constant.Router.login
        case .DMVanBan:
            return baseBgtURL + Constant.Router.login
        case .trangThaiDuThao:
            return baseBgtURL + Constant.Router.login
        case .VBChuaXuLy:
            return baseBgtURL + Constant.Router.login
        case .chiTietVBChuaXuLy:
            return baseBgtURL + Constant.Router.login
        case .duThaoChoDuyetKy:
            return baseBgtURL + Constant.Router.login
        case .chiTietDuThao:
            return baseBgtURL + Constant.Router.login

            // Văn bản + dự thảo actions
        case .xuLyXong:
            return baseBgtURL + Constant.Router.login
        case .chuyenVBDen:
            return baseBgtURL + Constant.Router.login
        case .duThaoTraLai:
            return baseBgtURL + Constant.Router.login
        case .duThaoKyPH:
            return baseBgtURL + Constant.Router.login
        case .duThaoKyPHDuyet:
            return baseBgtURL + Constant.Router.login

            //Thống kê
        case .thongKeDuThaoChoDuyetKy:
            return baseBgtURL + Constant.Router.login
        case .thongKeVanBanChuaXuLy:
            return baseBgtURL + Constant.Router.login
        }
//
    }
//    return baseBgtURL + Constant.Router.login

    
    /// The HTTP method used in the request.
    var method: HTTPMethod {
        switch self {
            
            // Login
        case .login:
            return .post
        case .userDetail:
            return .post
            
            // User Info
        case .canBoDetail:
            return .post
        case .T_Canbo:
            return .post
        case .capNhatCanBoDetail:
            return .post
        case .changepass:
            return .post
            
            // Văn bản + dự thảo dữ liệu
        case .layDanhSachFileDinhKem:
            return .get
        case .DMVanBan:
            return .post
        case .trangThaiDuThao:
            return .post
        case .VBChuaXuLy:
            return .post
        case .chiTietVBChuaXuLy:
            return .post
        case .duThaoChoDuyetKy:
            return .post
        case .chiTietDuThao:
            return .post
            
            // Văn bản + dự thảo actions
        case .xuLyXong:
            return .post
        case .chuyenVBDen:
            return .post
        case .duThaoTraLai:
            return .post
        case .duThaoKyPH:
            return .post
        case .duThaoKyPHDuyet:
            return .post
            
            //Thống kê
        case .thongKeDuThaoChoDuyetKy:
            return .post
        case .thongKeVanBanChuaXuLy:
            return .post
        }
        
    }
    
    /// The type of HTTP task to be performed.
    var task: Task {
        return .request
    }

}
